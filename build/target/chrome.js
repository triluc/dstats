/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const fs = require("fs");
const path = require("path");
const {promisify} = require("util");

const {google} = require("googleapis");
const request = require("request");

const readFileAsync = promisify(fs.readFile);

exports.addArguments = function(parser)
{
  parser.addArgument(["-c", "--credentials"], {required: true});
  parser.addArgument(["-p", "--package"], {required: false});
  parser.addArgument(["-n", "--name"], {defaultValue: "adblockpluschrome"});
  parser.addArgument(
    ["-v", "--verbose"],
    {defaultValue: false, type: Boolean, action: "storeTrue"}
  );
};

exports.run = async function(args)
{
  let version = args.package.match(/-(([\d+|.])+)\.\w+/)[1];
  let fileContent = await readFileAsync(path.resolve(args.credentials));
  let {
    clientId,
    clientSecret,
    appId,
    refreshToken
  } = JSON.parse(fileContent);

  let uploadUrl = `https://www.googleapis.com/upload/chromewebstore/v1.1/items/${appId}`;
  let publishUrl = `https://www.googleapis.com/chromewebstore/v1.1/items/${appId}/publish`;
  let statusUrl = `https://www.googleapis.com/chromewebstore/v1.1/items/${appId}?projection=draft`;

  let client = new google.auth.OAuth2(clientId, clientSecret);
  client.setCredentials({refresh_token: refreshToken});

  async function singleRequest(url, method, payload = null)
  {
    let headers = await client.getRequestHeaders(url);
    let options = {url, method, headers, json: true};

    let requestFnc = request;
    if (payload)
    {
      let packageReadStream = fs.createReadStream(path.resolve(args.package));
      requestFnc = (...opts) => packageReadStream.pipe(request(...opts));
    }

    let {response, body} = await new Promise((resolve, reject) =>
      requestFnc(options, (error, requestResponse, requestBody) =>
      {
        if (error) throw error;
        resolve({response: requestResponse, body: requestBody});
      })
    );
    if (response.statusCode != 200)
      throw response;
    return body;
  }

  async function waitForExpectedResponse(url, key, value, method)
  {
    // Repeat requests where an expected result may occur immediately or in
    // subsequent requests.
    function sleep(ms)
    {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    let waitForMS = 1;
    let repeat = false;

    do
    {
      repeat = false;
      try
      {
        let responseData = await singleRequest(url, method);
        repeat = responseData[key] != value;
      }
      catch (err)
      {
        // The CWS returns a 500 (Internal Server Error) when a package is
        // uploaded but not listed as a draft yet.
        if (err.statusCode == 500 && url == statusUrl)
          repeat = true;
        else
          throw err;
      }
      if (args.verbose)
        console.error(`Trying again in ${waitForMS / 1000} seconds...`);
      await sleep(waitForMS);
      waitForMS *= 2;
    } while (repeat);
  }

  try
  {
    // Check whether no previous package is in review, i.e. if we can push a
    // new package.
    if (args.verbose)
      console.error("Checking for possible publishing...");
    await singleRequest(publishUrl, "POST");
    // Upload the new package.
    if (args.verbose)
      console.error("Uploading " + args.package);
    await singleRequest(uploadUrl, "PUT", args.package);
    // Wait for the CWS to sign the package for publishing.
    if (args.verbose)
      console.error("Awaiting signing success...");
    await waitForExpectedResponse(statusUrl, "crxVersion", version, "GET");
    // Initiate the publishing. Note that the package might stay in "review
    // pending" afterwards.
    if (args.verbose)
      console.error("Initiating publishing...");
    await waitForExpectedResponse(publishUrl, "status", "OK", "POST");
  }
  catch (err)
  {
    // The Chrome Web Store returns a non-descriptive 400 (Bad Request)
    // when a previous item is not fully published yet. We can only guess
    // that this is the only cause for that error.
    if (err.statusCode == 400)
      console.error("Upload not possible. Is a package in review?");
    else
      console.error(err);
    process.exit(1);
  }
};

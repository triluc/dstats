DStats for Chrome, Opera and Firefox
========================================

Simply show how many bytes of Internet you wasted in the current month for
downloads.

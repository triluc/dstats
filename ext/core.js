"use strict";

function onError(error)
{
  console.error(`Error: ${error}`);
}

function beautifybytes(bytes)
{
  let tmp = bytes;
  let index = 0;
  let units = ["Byte", "Kilobyte", "Megabyte", "Gigabyte", "Terrabyte",
               "Petabyte"];
  while (tmp > 1000)
  {
    index += 1;
    tmp /= 1000;
  }

  return tmp.toFixed(3) + " " + units[index];
}

function initializeLatestDownload(downloadItems)
{
  let sizeView = document.querySelector("#bytes");
  let countView = document.querySelector("#ndownloads");
  let totalCount = 0;
  for (let download of downloadItems)
  {
    totalCount += download.totalBytes;
  }
  sizeView.textContent = beautifybytes(totalCount);
  countView.textContent = downloadItems.length;
}

/*
Search for the most recent download, and pass it to initializeLatestDownload()
*/
function getDownloads()
{
  let browserApi = null;
  try
  {
    browserApi = browser;
  }
  catch (err)
  {
    browserApi = chrome;
  }
  let month = new Date();
  month.setMinutes(0);
  month.setHours(0);
  month.setDate(0);
  month.setSeconds(0);
  month.setMilliseconds(0);
  browserApi.downloads.search({
    endedAfter: month.toISOString()
  }, initializeLatestDownload);
}

getDownloads();
